package neu.mr6240.request;

/**
 * Enum class for the columns in the request file
 * @author prasadmemane
 */
public enum RequestIndex {
	
	YEAR(0), MONTH(1), DAY(2), ORIGIN(3), DEST(4);
	
	private int value;

	private RequestIndex(int value) {
		this.value = value;
	}

	public int val() {
		return value;
	}

}

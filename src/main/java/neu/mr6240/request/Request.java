package neu.mr6240.request;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.zip.GZIPInputStream;

import org.joda.time.LocalDate;

import com.google.common.collect.Multimap;
import com.google.common.collect.Table;

import au.com.bytecode.opencsv.CSVParser;
import neu.mr6240.missed.Flight;
import neu.mr6240.utils.ConnectionComparator;
import neu.mr6240.utils.ConnectionDetails;

/**
 * This is class for processing the request file and writing the itineraries
 * based on the prediction
 * 
 * @author prasad memane
 * @author swapnil mahajan
 * @author ajay subramanya
 * @author smitha banglorenaresh
 */
public class Request {

	private static final String ITINERARY_FILE = "Itinerary.txt";
	private static int PENALTY = 100 * 60;
	private static int NO_OF_REQUESTS = 10000;	
	private static int HUNDRED = 100;
	private static final double HOUR = 60.0;
	private static final double DOUBLE_ONE = 1.0;

	private DecimalFormat d2 = new DecimalFormat("#.##");
	private String requestFile;
	int missedCounter = 0;
	int sumDuration = 0;
	CSVParser parser;

	public Request(String requestFile) {
		this.requestFile = requestFile;
		this.parser = new CSVParser();
	}

	/**
	 * This method processes the request file and writes out the itineraries
	 * based on the prediction. If for a particular request, there is no
	 * connection, nothing is written out
	 * 
	 * @param connections
	 * @throws FileNotFoundException
	 * @throws IOException
	 */
	public void writeItinerary(Table<String, String, Multimap<LocalDate, ConnectionDetails>> connections,
			Table<Integer, Integer, ArrayList<Flight>> missedFlights) throws FileNotFoundException, IOException {
		BufferedReader br = new BufferedReader(
				new InputStreamReader(new GZIPInputStream(new FileInputStream(requestFile))));

		BufferedWriter bw = new BufferedWriter(new FileWriter(new File(ITINERARY_FILE)));

		String line;
		System.out.println("writting itineraries ...");
		while ((line = br.readLine()) != null) {
			String[] values = parser.parseLine(line);

			LocalDate date = new LocalDate(Integer.parseInt(values[RequestIndex.YEAR.val()]),
					Integer.parseInt(values[RequestIndex.MONTH.val()]),
					Integer.parseInt(values[RequestIndex.DAY.val()]));
			String org = values[RequestIndex.ORIGIN.val()];
			String dest = values[RequestIndex.DEST.val()];

			if (connections.contains(org, dest)) {
				if (connections.get(org, dest).containsKey(date)) {
					writeConnection(date, org, dest, connections, bw, missedFlights);
				}
			}
		}
		System.out.println("Missed Flights: " + missedCounter);
		System.out.println("Total Duration in Minutes: " + sumDuration);
		System.out.println("Total Duration in Hours: " + d2.format(sumDuration/HOUR));
		System.out.println("Accuracy: " + d2.format(((NO_OF_REQUESTS - missedCounter) * HUNDRED) / (NO_OF_REQUESTS * DOUBLE_ONE)));
		br.close();
		bw.close();
	}

	/**
	 * This method writes the connection
	 * 
	 * @param date
	 * @param org
	 * @param dest
	 * @param connections
	 * @param bw
	 * @throws IOException
	 */
	private void writeConnection(LocalDate date, String org, String dest,
			Table<String, String, Multimap<LocalDate, ConnectionDetails>> connections, BufferedWriter bw,
			Table<Integer, Integer, ArrayList<Flight>> missedFlights) throws IOException {

		List<ConnectionDetails> missed = new ArrayList<>();
		List<ConnectionDetails> conn = new ArrayList<>();
		split(connections.get(org, dest).get(date), missed, conn);

		ConnectionDetails minConn = null;
		if (!conn.isEmpty()) {
			minConn = Collections.min(conn, new ConnectionComparator());
		} else if (!missed.isEmpty()) {
			minConn = Collections.min(missed, new ConnectionComparator());			
		}

		int duration = checkMissedForScore(minConn, missedFlights, date);
		sumDuration += duration;
		bw.write(minConn.getOrgConnect() + ", " + minConn.getConnectDest() + ", " + duration + "\n");
	}

	/**
	 * This method splits the collection of ConnectionDetails objects, for a
	 * specific date, origin and destination based on if the connection was
	 * missed or not
	 * 
	 * @param cd
	 * @param missed
	 * @param conn
	 */
	private void split(Collection<ConnectionDetails> cd, List<ConnectionDetails> missed, List<ConnectionDetails> conn) {
		for (ConnectionDetails c : cd) {
			if (c.isMissed())
				missed.add(c);
			else
				conn.add(c);
		}
	}

	/**
	 * This method checks if the itinerary we created was missed and produces a
	 * score/duration based on it. If the itinerary was not missed: duration is
	 * the duration of the itinerary If the itinerary was missed: duration is
	 * the duration of the itinerary + 100
	 * 
	 * @param minConn
	 * @param missedFlights
	 * @return
	 */
	private int checkMissedForScore(ConnectionDetails minConn, Table<Integer, Integer, ArrayList<Flight>> missedFlights,
			LocalDate date) {
		int duration = minConn.getDuration();
		int f1 = minConn.getOrgConnect();
		int f2 = minConn.getConnectDest();
		if (missedFlights.contains(f1, f2)) {
			for (Flight f : missedFlights.get(f1, f2)) {
				if (f.getYear() == date.getYear() && f.getMonth() == date.getMonthOfYear()
						&& f.getDay() == date.getDayOfMonth()) {
					duration += PENALTY;
					missedCounter++;
					break;
				}
			}
		}
		return duration;
	}
}

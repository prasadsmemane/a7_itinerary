package neu.mr6240.missed;

/**
 * This is a class for holding the missed flight details
 * @author prasadmemane
 *
 */
public class Flight {
	
	private int year;
	private short month;
	private short day;
	private String origin;
	private String dest;
	private int f1;
	private int f2;

	public Flight(int year, short month, short day, String origin, String dest, int f1, int f2) {
		this.year = year;
		this.month = month;
		this.day = day;
		this.origin = origin;
		this.dest = dest;
		this.f1 = f1;
		this.f2 = f2;
	}
	
	@Override
	public String toString() {
		return "Flights [year=" + year + ", month=" + month + ", day=" + day + ", origin=" + origin + ", dest=" + dest
				+ ", f1=" + f1 + ", f2=" + f2 + "]";
	}

	public int getYear() {
		return year;
	}

	public void setYear(int year) {
		this.year = year;
	}

	public short getMonth() {
		return month;
	}

	public void setMonth(short month) {
		this.month = month;
	}

	public short getDay() {
		return day;
	}

	public void setDay(short day) {
		this.day = day;
	}

	public String getOrigin() {
		return origin;
	}

	public void setOrigin(String origin) {
		this.origin = origin;
	}

	public String getDest() {
		return dest;
	}

	public void setDest(String dest) {
		this.dest = dest;
	}
	
	public int getF1() {
		return f1;
	}

	public void setF1(int f1) {
		this.f1 = f1;
	}

	public int getF2() {
		return f2;
	}

	public void setF2(int f2) {
		this.f2 = f2;
	}

}

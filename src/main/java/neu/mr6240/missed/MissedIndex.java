package neu.mr6240.missed;

/**
 * Enum class for the columns in the missed file
 * @author prasadmemane
 */
public enum MissedIndex {

	YEAR(0), MONTH(1), DAY(2), ORIGIN(3), DEST(4), ORG_CONNECT(5), CONNECT_DEST(6);
	
	private int value;

	private MissedIndex(int value) {
		this.value = value;
	}

	public int val() {
		return value;
	}

}

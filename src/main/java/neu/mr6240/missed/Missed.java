package neu.mr6240.missed;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.zip.GZIPInputStream;

import com.google.common.collect.HashBasedTable;
import com.google.common.collect.Table;

import au.com.bytecode.opencsv.CSVParser;

/**
 * This class processes the missed file, 04missed.csv.gz and puts them in a data structure
 * which is required while scoring for each request in the request file for which we are generating
 * the itineraries
 * @author prasadmemane
 * @author swapnil mahajan
 * @author ajay subramanya
 * @author smitha banglorenaresh
 */
public class Missed {
	
	private String missedFile;
	Table<Integer, Integer, ArrayList<Flight>> missedFlights;
	CSVParser parser;
	
	public Missed(String missedFile) {
		this.missedFile = missedFile;
		this.missedFlights = HashBasedTable.create();
		this.parser = new CSVParser();
	}
	
	/**
	 * This method builds the Table data structure for the missed flights present in the 
	 * 04missed.csv.gz file
	 * @throws FileNotFoundException
	 * @throws IOException
	 */
	public void buildMissedDS() throws FileNotFoundException, IOException {
		BufferedReader br = new BufferedReader(
				new InputStreamReader(new GZIPInputStream(new FileInputStream(missedFile))));
		
		String line;		
		while((line = br.readLine()) != null) {
			String[] values = parser.parseLine(line);
			
			//Sanity check
			if (values != null && values.length == 7) {
				int f1 = Integer.parseInt(values[MissedIndex.ORG_CONNECT.val()]);
				int f2 = Integer.parseInt(values[MissedIndex.CONNECT_DEST.val()]);
				Flight flight = new Flight(Integer.parseInt(values[MissedIndex.YEAR.val()]), Short.parseShort(values[MissedIndex.MONTH.val()]),
											Short.parseShort(values[MissedIndex.DAY.val()]), values[MissedIndex.ORIGIN.val()], 
											values[MissedIndex.DEST.val()], f1, f2);
				
				addToTable(f1, f2, flight);				
			}			
		}
		
		br.close();
	}
	
	/**
	 * This method adds the Flight details to the missedFlights table
	 * @param f1
	 * @param f2
	 * @param flight
	 */
	public void addToTable(int f1, int f2, Flight flight) {
		if (missedFlights.contains(f1, f2)) {
			missedFlights.get(f1, f2).add(flight);
		} 
		else {
			ArrayList<Flight> allFlights = new ArrayList<>();
			allFlights.add(flight);
			missedFlights.put(f1, f2, allFlights);
		}
	}

	public Table<Integer, Integer, ArrayList<Flight>> getMissedFlights() {
		return missedFlights;
	}

	public void setMissedFlights(
			Table<Integer, Integer, ArrayList<Flight>> missedFlights) {
		this.missedFlights = missedFlights;
	}
}

package neu.mr6240.utils;

/**
 * This class processes the command line arguments
 * @author prasadmemane
 */
public class CLI {
	public static final int REQUEST_FILE = 0;
	public static final int PREDICTION_FILE = 1;
	public static final int MISSED_FILE = 2;
	
	public static final String CMD_SEPERATOR = "=";

	private String[] args;
	
	private String requestFile;
	private String predictionFile;
	private String missedFile;

	public CLI(String[] args) {
		this.args = args;
		setRequestFile(args[REQUEST_FILE]);
		setPredictionFile(args[PREDICTION_FILE]);
		setMissedFile(args[MISSED_FILE]);
	}

	public String[] getArgs() {
		return args;
	}

	public void setArgs(String[] args) {
		this.args = args;
	}

	public String getRequestFile() {
		return requestFile;
	}

	/**
	 * Set the argument for parameter "request="
	 * @param requestFile
	 */
	public void setRequestFile(String requestFile) {
		this.requestFile = requestFile.substring(requestFile.indexOf(CMD_SEPERATOR) + 1);
	}

	public String getPredictionFile() {
		return predictionFile;
	}

	/**
	 * Set the argument for parameter "prediction="
	 * @param predictionFile
	 */
	public void setPredictionFile(String predictionFile) {
		this.predictionFile = predictionFile.substring(predictionFile.indexOf(CMD_SEPERATOR) + 1);
	}

	public String getMissedFile() {
		return missedFile;
	}

	/**
	 * Set the argument for parameter "missed="
	 * @param missedFile
	 */
	public void setMissedFile(String missedFile) {
		this.missedFile = missedFile.substring(missedFile.indexOf(CMD_SEPERATOR) + 1);;
	}

}

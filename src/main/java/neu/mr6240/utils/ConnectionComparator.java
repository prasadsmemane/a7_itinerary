package neu.mr6240.utils;

import java.util.Comparator;

/**
 * This is the comparator class for comparing based on the duration filed
 * in the ConnectionDetails object
 * @author prasadmemane
 */
public class ConnectionComparator implements Comparator<ConnectionDetails> {

	@Override
	public int compare(ConnectionDetails o1, ConnectionDetails o2) {
		return new Integer(o1.getDuration()).compareTo(new Integer(o2.getDuration()));
	}

}

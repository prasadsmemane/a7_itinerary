package neu.mr6240.utils;

/**
 * This is a POJO class for itineraries
 * @author prasadmemane
 */
public class ConnectionDetails {
	
	private int orgConnect;
	private int connectDest;
	private boolean missed;
	private int duration;
	
	public ConnectionDetails(int orgConnect, int connectDest, boolean missed, int duration) {		
		this.orgConnect = orgConnect;
		this.connectDest = connectDest;
		this.missed = missed;
		this.duration = duration;
	}

	public int getOrgConnect() {
		return orgConnect;
	}
	
	public void setOrgConnect(int orgConnect) {
		this.orgConnect = orgConnect;
	}
	
	public int getConnectDest() {
		return connectDest;
	}
	
	public void setConnectDest(int connectDest) {
		this.connectDest = connectDest;
	}
	
	public boolean isMissed() {
		return missed;
	}
	
	public void setMissed(boolean missed) {
		this.missed = missed;
	}
	
	public int getDuration() {
		return duration;
	}
	
	public void setDuration(int duration) {
		this.duration = duration;
	}

	@Override
	public String toString() {
		return "ConnectionDetails [orgConnect=" + orgConnect + ", connectDest="
				+ connectDest + ", missed=" + missed + ", duration=" + duration
				+ "]";
	}
	
}

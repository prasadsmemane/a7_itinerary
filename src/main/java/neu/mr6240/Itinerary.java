package neu.mr6240;

import java.io.FileNotFoundException;
import java.io.IOException;

import neu.mr6240.missed.Missed;
import neu.mr6240.prediction.Prediction;
import neu.mr6240.request.Request;
import neu.mr6240.utils.CLI;

/**
 * This is the main class that process the request file and based on prediction
 * creates itineraries
 * 
 * @author prasad memane
 * @author swapnil mahajan
 * @author ajay subramanya
 * @author smitha banglorenaresh
 * 
 */
public class Itinerary {
	public static void main(String[] args) {
		CLI cli = new CLI(args);

		Prediction pred = new Prediction(cli.getPredictionFile());
		try {
			System.out.println("reading predictions ...");
			pred.buildDataSet();
		} catch (IOException e) {
			System.err
					.println("IOException while processing the prediction file: " + cli.getPredictionFile() + " " + e);
		}

		Missed miss = new Missed(cli.getMissedFile());
		try {
			System.out.println("reading missed flights ...");
			miss.buildMissedDS();
		} catch (FileNotFoundException e) {
			System.err.println("missed file not present: " + cli.getMissedFile() + " " + e);
		} catch (IOException e) {
			System.err.println("IOException while processing the missed file: " + cli.getMissedFile() + " " + e);
		}

		Request req = new Request(cli.getRequestFile());
		try {
			System.out.println("processing requests ...");
			req.writeItinerary(pred.getConnections(), miss.getMissedFlights());
		} catch (FileNotFoundException e) {
			System.err.println("request file not present: " + cli.getRequestFile() + " " + e);
		} catch (IOException e) {
			System.err.println("IOException while processing the request file: " + e);
		}
	}
}

package neu.mr6240.prediction;

/**
 * Enum class for the columns in the prediction file
 * @author prasadmemane
 */
public enum PredictionIndex {
	
	CARRIER(0), ORIGIN(1), CONN(2), DEST(3), YEAR(4), MONTH(5), DAY(6), DURATION(7), MISSED(8), ORG_CONNECT(9), CONNECT_DEST(10);
	
	private int value;

	private PredictionIndex(int value) {
		this.value = value;
	}

	public int val() {
		return value;
	}
	
}

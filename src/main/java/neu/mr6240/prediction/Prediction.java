package neu.mr6240.prediction;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

import neu.mr6240.utils.ConnectionDetails;

import org.joda.time.LocalDate;

import au.com.bytecode.opencsv.CSVParser;

import com.google.common.collect.HashBasedTable;
import com.google.common.collect.HashMultimap;
import com.google.common.collect.Multimap;
import com.google.common.collect.Table;

/**
 * This is the class which processes the prediction file written by the mapreduce jobs
 * and puts them in a data structure which is then used by the request file entries to create
 * itineraries
 * @author prasad memane
 * @author swapnil mahajan
 * @author ajay subramanya
 * @author smitha banglorenaresh
 */
public class Prediction {

	private String predictionFile;
	private Table<String, String, Multimap<LocalDate, ConnectionDetails>> connections;
	CSVParser parser;
		
	public Prediction(String predictionFile) {
		this.predictionFile = predictionFile;
		this.connections = HashBasedTable.create();;
		this.parser = new CSVParser();
	}
	
	/**
	 * This method builds the data structure based on the prediction file required to create itineraries
	 * for the entries in the request file
	 * @throws IOException
	 */
	public void buildDataSet() throws IOException {				
		BufferedReader br = new BufferedReader(new FileReader(predictionFile));
		
		String line;
		while((line = br.readLine()) != null) {
			String[] values = parser.parseLine(line);
			
			LocalDate date = new LocalDate(Integer.parseInt(values[PredictionIndex.YEAR.val()]), 
					Integer.parseInt(values[PredictionIndex.MONTH.val()]), Integer.parseInt(values[PredictionIndex.DAY.val()]));			
			String org = values[PredictionIndex.ORIGIN.val()];
			String dest = values[PredictionIndex.DEST.val()];
			int orgConnect = Integer.parseInt(values[PredictionIndex.ORG_CONNECT.val()]);
			int connectDest = Integer.parseInt(values[PredictionIndex.CONNECT_DEST.val()]);
			boolean missed = Boolean.parseBoolean(values[PredictionIndex.MISSED.val()]);
			int duration = Integer.parseInt(values[PredictionIndex.DURATION.val()]);
			
			if(connections.contains(org, dest)) {				
				Multimap<LocalDate, ConnectionDetails> hm = connections.get(org, dest);
				hm.put(date, new ConnectionDetails(orgConnect, connectDest, missed, duration));
				connections.put(org, dest, hm);								
			}
			else {
				Multimap<LocalDate, ConnectionDetails> hm = HashMultimap.create();
				hm.put(date, new ConnectionDetails(orgConnect, connectDest, missed, duration));
				connections.put(org, dest, hm);				
			}			
		}
						
		br.close();
	}

	public String getPredictionFile() {
		return predictionFile;
	}

	public void setPredictionFile(String predictionFile) {
		this.predictionFile = predictionFile;
	}

	public Table<String, String, Multimap<LocalDate, ConnectionDetails>> getConnections() {
		return connections;
	}

	public void setConnections(
			Table<String, String, Multimap<LocalDate, ConnectionDetails>> connections) {
		this.connections = connections;
	}	
	
}
